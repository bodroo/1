/** Создайте массив из всех чётных чисел от 2 до 20 и выведите элементы массива 
на экран сначала в строку, отделяя один элемент от другого пробелом, 
а затем в столбик (отделяя один элемент от другого началом новой строки). 
Перед созданием массива подумайте, какого он будет размера. 
*/ 
package java3;
/**
 *
 * @author mei1142b
 */
public class Java3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int b=0;
        int[] a= new int[10]; //Объявляем массив
        for (int i=0;i<10;i++){ 
            b=b+2;
            a[i]=b;
            System.out.print(a[i]+" "); // Вывод элементов массива в строку
           
        }
        System.out.println();
       for (int i=0;i<10;i++){
          
             System.out.println(a[i]); // Вывод элементов массива в столбец    
    }
    }
}
